package PT2019.hw1Polynomial.HW1_Polynomial;

public class QRPair {
	private Polynomial quotient;
	private Polynomial remainder;
	
	public QRPair() {
		quotient = new Polynomial();
		remainder = new Polynomial();
	}
	
	public static QRPair dividePolynomials(Polynomial a, Polynomial b) throws IllegalArgumentException {
		if (b.size() == 1 && b.get(0).coef() == 0 && b.get(0).power() == 0) {
			throw new IllegalArgumentException("Cannot divide by 0");
		}
		
		QRPair qr = new QRPair();
		
		for(Monomial i : a) {
			qr.remainder.add(i);
		}

		int powerB = b.get(0).power();
		System.out.println(qr.remainder.get(0).power());
		Monomial x;
		Polynomial t;
		
		while (qr.remainder.size() > 0 && qr.remainder.get(0).power() >= powerB) {
			x = new Monomial(qr.remainder.get(0).coef() / b.get(0).coef(), 
							 qr.remainder.get(0).power() - b.get(0).power());
			
			qr.quotient.add(x);

			t = new Polynomial();
			t.add(x);
			t = Polynomial.multiply(t, b);
			
			qr.remainder = qr.remainder.substract(t);
		}
		
		return qr;
	}
	
	public String toString() {
		return "Q: " + quotient.toString() + "\n  R: " + remainder.toString();
	}
	
	public Polynomial getQuotient() {
		return quotient;
	}
	
	public Polynomial getRemainder() {
		return remainder;
	}
}
