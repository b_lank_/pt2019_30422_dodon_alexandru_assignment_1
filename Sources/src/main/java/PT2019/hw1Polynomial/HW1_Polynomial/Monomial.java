package PT2019.hw1Polynomial.HW1_Polynomial;

public class Monomial {
	private int power;
	private float coeficient;
	
	public Monomial(float c, int p) {
		coeficient = c;
		power = p;
	}
	
	public static Monomial add(Monomial a, Monomial b) {
		if (a.power() != b.power() || a.coef() + b.coef() == 0) {
			return null;
		}
		
		return new Monomial(a.coef() + b.coef(), a.power());		
	}
	
	public static Monomial multiply(Monomial a, Monomial b) {
		return new Monomial(a.coef()*b.coef(), a.power() + b.power());
	}
	
	public int power() {
		return power;
	}
	
	public float coef() {
		return coeficient;
	}
}
