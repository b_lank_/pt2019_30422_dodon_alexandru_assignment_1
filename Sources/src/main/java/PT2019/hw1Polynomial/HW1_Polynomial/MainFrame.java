package PT2019.hw1Polynomial.HW1_Polynomial;
import java.awt.*;
import java.util.concurrent.TimeUnit;
import java.awt.event.*;
import java.util.Arrays;
import javax.swing.*;
/**
 * 
 */

/**
 * The window seen by the user
 *
 */
public class MainFrame extends JFrame {	
	
	public MainFrame() {
		setVisible(true);
		setSize(1200, 300);
		setTitle("Polynomial processing");
        this.setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
		Container c = getContentPane();
		
		Box b = Box.createVerticalBox();
		Box b1 = Box.createHorizontalBox();
		Box b2 = Box.createHorizontalBox();
		Box b3 = Box.createHorizontalBox();
		Box b4 = Box.createHorizontalBox();
		

		JLabel l1 = new JLabel("P1 = ");
		JLabel l2 = new JLabel("P2 = ");
		final JTextField p1 = new JTextField();
		final JTextField p2 = new JTextField();
		
		final String[] opOptions = {"Add", "Substract P2 from P1", "Substract P1 from P2", "Multiply", 
				"Divide P1 by P2", "Divide P2 by P1", "Derive P1", "Derive P2", "Integrate P1", "Integrate P2"};
		final JComboBox operation = new JComboBox(opOptions);
		operation.setMaximumSize(new Dimension(1000, 150));
		JButton eval = new JButton("Evaluate");
		
		JLabel lr = new JLabel("Result =");
		final JTextField res = new JTextField();
		res.setEditable(false);
		
		b1.add(l1);
		b1.add(p1);
		b1.setMaximumSize(new Dimension(1000, 150));
		b2.add(l2);
		b2.add(p2);
		b2.setMaximumSize(new Dimension(1000, 150));
		b3.add(eval);
		b3.add(operation);
		b3.setMaximumSize(new Dimension(1000, 50));
		b4.add(lr);
		b4.add(res);
		b4.setMaximumSize(new Dimension(1000, 150));
		b.add(b1);
		b.add(b2);
		b.add(b3);
		b.add(b4);
		this.add(b);

		eval.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
					String result = App.evaluate((String) operation.getSelectedItem(), p1.getText(), p2.getText());
			
					res.setText(result);
			}
		});
	} 
}
