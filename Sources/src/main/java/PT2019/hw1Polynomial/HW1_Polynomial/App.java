package PT2019.hw1Polynomial.HW1_Polynomial;

import javax.swing.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainFrame mf = new MainFrame();
			}
		});
    }
    
    public static String evaluate(String operation, String p1, String p2) {
    	try {
	    	switch (operation) {
	    	case "Add":
	    		return Polynomial.add(new Polynomial(p1), new Polynomial(p2)).toString();
	    		
	    	case "Substract P2 from P1":
	    		return new Polynomial(p1).substract(new Polynomial(p2)).toString();
	    		
	    	case "Substract P1 from P2":
	    		return new Polynomial(p2).substract(new Polynomial(p1)).toString();
	    		
	    	case "Multiply":
	    		return Polynomial.multiply(new Polynomial(p1), new Polynomial(p2)).toString();
	    		
	    	case "Divide P1 by P2":
	    		try {
	        		return QRPair.dividePolynomials(new Polynomial(p1), new Polynomial(p2)).toString();
	    		} 
	    		catch (IllegalArgumentException e) {
	    			return "Illegal argument. Most likely trying to divide by 0.";
	    		}
	    		
	    	case "Divide P2 by P1":
	    		try {
	        		return QRPair.dividePolynomials(new Polynomial(p2), new Polynomial(p1)).toString();
	    		}
	    		catch (IllegalArgumentException e) {
	    			return "Illegal argument. Most likely trying to divide by 0.";
	    		}
	    		
	    	case "Derive P1":
	    		return new Polynomial(p1).derivative().toString();
	    		
	    	case "Derive P2":
	    		return new Polynomial(p2).derivative().toString();
	    		
	    	case "Integrate P1":
	    		return new Polynomial(p1).integral().toString();
	
	    	case "Integrate P2":
	    		return new Polynomial(p2).integral().toString();
	    		
	    	default:
	    		return "Unexpected command";
	    	}
    	}
    	catch(Exception e) {
    		return "Check the inputs";
    	}
    }
}
