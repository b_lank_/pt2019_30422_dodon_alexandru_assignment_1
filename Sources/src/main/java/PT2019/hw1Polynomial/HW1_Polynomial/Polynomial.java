package PT2019.hw1Polynomial.HW1_Polynomial;

import java.util.ArrayList;
import java.util.regex.*;

public class Polynomial extends ArrayList<Monomial> {

	public Polynomial() {}
	
	private Polynomial(Monomial m) {
		add(m);
	}
	
	public Polynomial(String s) {
		Pattern p = Pattern.compile("(\\s*[-+]?\\s*\\d*\\s*)?(x|\\*x)?(\\s*\\^\\s*(\\s*[-+]?\\s*\\d*))?", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(s);
		Polynomial aux = new Polynomial();
		Monomial z;
		String g1, g2, g4;
		float coef;
		int power;
		
		Pattern check = Pattern.compile("^x",Pattern.CASE_INSENSITIVE);
		
		while (m.find()) {
			if (m.group().length() != 0) {
				g1 = m.group(1);
				g2 = m.group(2);
				g4 = m.group(4);
				
				if (g2 != null && g2.length() > 0) {//got x
					if (g4 != null && g4.length() > 0) {//got exponent
						g4 = g4.replace(" ", "");
						power = Integer.parseInt(g4);
					} else {//implicit exponent 1
						power = 1;
					}
					
					if (g1 != null && g1.length() > 0) {//got coef
						g1 = g1.replace(" ", "");
						if (g1.compareTo("-") == 0) {//implicit coef -1
							coef = -1;
						} else if (g1.compareTo("+") == 0) {//implicit coef 1
							coef = 1;
						} else {
							coef = Integer.parseInt(g1);
						}
					} else {//implicit coef 1
						coef = 1;
					}
				} else {//just a constant, implicit exponent 0
					power = 0;
					g1 = g1.replace(" ", "");
					coef = Integer.parseInt(g1);
				}
				
				if (coef == 0) {
					z = new Monomial(coef, 0);
				} else {
					z = new Monomial(coef, power);
				}
				
				aux = Polynomial.add(aux, new Polynomial(z));
			}
		}
		
		for (Monomial i : aux) {
			this.add(i);
		}
		
		if (this.size() == 0) {
			this.add(new Monomial(0,0));
		}
	}
	
	public String toString() {
		String s = new String();
		boolean checkFirst = true;
		
		for(Monomial i : this) {
				if (checkFirst) {
					checkFirst = false;
					
					if (i.coef() < 0) {
						s += "-";
					}
				} else if (i.coef() < 0) {
					s += " - ";
				} else {
					s += " + ";
				}
		
				if (i.power() != 0) {
					if (i.coef() != 1 && i.coef() != -1) {
						if (Math.round(i.coef()) == i.coef()) {
							s += Integer.toString(Math.abs((int) i.coef()));
						} else {
							s += Double.toString(Math.abs( Math.round(i.coef() * 100) / 100.0));
						}
					}
					
					s += "x";
					
					if (i.power() != 1) {
						s += "^" + Integer.toString(i.power());
					}
				} else {
					if (Math.round(i.coef()) == i.coef()) {
						s += Integer.toString(Math.abs((int) i.coef()));
					} else {
					s += Double.toString(Math.abs( Math.round(i.coef() * 100) / 100.0));
					}
				}
		}
		
		return s;
	}
	
	public static Polynomial add(Polynomial a, Polynomial b) {
		Polynomial c = new Polynomial();
		Monomial x, u, v;
		int indexB = 0, indexA = 0, sizeA = a.size(), sizeB = b.size();
		
		while (indexA < sizeA && indexB < sizeB) {
			u = a.get(indexA);
			v = b.get(indexB);
			
			if (u.power() > v.power()) {
				c.add(u);
				indexA++;
			} else if (v.power() > u.power()) {
				c.add(v);
				indexB++;
			} else {
				x = Monomial.add(u, v);
				
				if (x != null) {
					c.add(x);
				}
				
				indexA++;
				indexB++;
			}
		}
		
		while (indexA < sizeA) {
			c.add(a.get(indexA));
			indexA++;
		}
		while (indexB < sizeB) {
			c.add(b.get(indexB));
			indexB++;
		}
		
		return c;
	}

	public static Polynomial multiply(Polynomial a, Polynomial b) {
		Polynomial c = new Polynomial();
		
		if ((a.get(0).power() == 0 && a.get(0).coef() == 0 && a.size() == 1) || (b.get(0).power() == 0 && b.get(0).coef() == 0 && b.size() == 1)) {
			c.add(new Monomial(0, 0));
			
			return c;
		}
		Monomial x, z;
		
		for (Monomial i : a) {
			for (Monomial j : b) {
				x = Monomial.multiply(i, j);
				
				for (int l = 0, sizeC = c.size(); l <= sizeC; l++) {
					if (l == sizeC) {
						c.add(x);
					} else if (x.power() > c.get(l).power()) {
						c.add(l, x);
						
						break;
					} else if (x.power() == c.get(l).power()) {
						z = Monomial.add(x, c.get(l));
						
						if (z == null) {
							c.remove(l);
						} else {
							c.set(l, z);
						}
						
						break;
					}
				}
			}
 		}
		
		return c;
	}

	public Polynomial substract(Polynomial b) {
		return Polynomial.add(this, Polynomial.multiply(b, new Polynomial(new Monomial(-1,0))));		
	}

	public Polynomial derivative() {
		Polynomial c = new Polynomial();
		
		if (get(0).power() == 0 && size() == 1) {
			c.add(new Monomial(0, 0));
		}
		
		for(Monomial i : this) {
			if (i.power() != 0) {
				c.add(new Monomial(i.coef() * i.power(), i.power() - 1));
			}
		}
		
		return c;
	}
	
	public Polynomial integral() {
		Polynomial c = new Polynomial();
		
		if (get(0).power() == 0 && get(0).coef() == 0 && size() == 1) {
			c.add(new Monomial(0, 0));
			
			return c;
		}
		
		Monomial x;
		
		for(Monomial i : this) {
			x = new Monomial(i.coef() / (i.power() + 1), i.power() + 1);
			
			if (x.coef() != 0) {
				c.add(x);	
			}
		}
		
		return c;
	}
}