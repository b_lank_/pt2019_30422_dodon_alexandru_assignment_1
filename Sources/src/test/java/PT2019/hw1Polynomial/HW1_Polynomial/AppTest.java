package PT2019.hw1Polynomial.HW1_Polynomial;

import org.junit.Assert;
import org.junit.Test;

public class AppTest {
	
	Polynomial firstPolinom=new Polynomial("x^3 - 4x^2 + 7x - 5");
	Polynomial secondPolinom=new Polynomial("x + 2");
	
	
	@Test
	
	public void addition()
	{	
		Assert.assertTrue(Polynomial.add(firstPolinom, secondPolinom).toString().equals("x^3 - 4x^2 + 8x - 3"));
	}
	@Test
	public void substraction()
	{		
		Assert.assertTrue(firstPolinom.substract(secondPolinom).toString().equals("x^3 - 4x^2 + 6x - 7"));
		
	}
	@Test 
	public void multiplication()
	{		
		Assert.assertTrue(Polynomial.multiply(firstPolinom,secondPolinom).toString().equals("x^4 - 2x^3 - x^2 + 9x - 10"));
	}
	
	@Test
	public void division()
	{
		try {
			QRPair qr = QRPair.dividePolynomials(firstPolinom, new Polynomial("0"));
		}
		catch(Exception e) {
		Assert.assertTrue(true);
		}
	}
	
	@Test
	public void derivative()
	{	
		Assert.assertTrue(secondPolinom.derivative().toString().equals("1"));
		
	}
	
	@Test
	public void integral()
	{	
		Assert.assertTrue(secondPolinom.integral().toString().equals("0.5x^2 + 2x"));
	}
	
}